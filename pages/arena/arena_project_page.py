from selenium.webdriver.common.by import By
from selenium.webdriver.ie.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions


class ArenaProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def verify_title(self, title):
        title = self.browser.find_element(By.CSS_SELECTOR, '.content_title')
        assert title.text == 'Projekty'

    def click_add_project_button(self):
        self.browser.find_element(By.CSS_SELECTOR,
                                  'a.button_link[href="http://demo.testarena.pl/administration/add_project"]').click()  # Czy taki selektor będzie bezpieczny, gdyby adres URL uległ zmianie?

    def insert_project_name(self, project_name):
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(project_name)

    def insert_project_prefix(self, project_prefix):
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(project_prefix)

    def insert_project_description(self, project_description):
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(project_description)

    def click_save_button(self):
        self.browser.find_element(By.CSS_SELECTOR, '#save').click()

    def wait_for_success_message_on_popup(self):
        wait = WebDriverWait(self.browser, 10)
        text_to_be_awaited = (By.CSS_SELECTOR, '#j_info_box p')
        wait.until(expected_conditions.text_to_be_present_in_element(text_to_be_awaited, 'Projekt został dodany.'))

    def verify_project_success_added_popup(self):
        popup_message = self.browser.find_element(By.CSS_SELECTOR, '#j_info_box p')
        assert popup_message.text == 'Projekt został dodany.'

    def click_projects_tab_from_administration_page(self):
        self.browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()

    def insert_text_to_search_input(self, project_name):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(project_name)

    def click_search_button(self):
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()
