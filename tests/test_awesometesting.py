import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

@pytest.fixture
def browser():
    # 1 - To wykona się PRZED każdym testem, którzy korzysta z tego fixture'a
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('https://awesome-testing.blogspot.com')
    # 2 - To jest to co zwracamy (przekazujemy) do testów
    yield browser
    # 3 - To wykona się PO każdym testem, którzy korzysta z tego fixture'a
    browser.quit()

def test_post_count(browser):

    # Pobranie listy tytułów
    list_of_titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 4 elementy
    assert len(list_of_titles) == 4




def test_post_count_after_search(browser):

    # Inicjalizacja searchbara i przycisku search
    search_bar = browser.find_element(By.CSS_SELECTOR, 'input.gsc-input')
    search_button = browser.find_element(By.CSS_SELECTOR, '.gsc-search-button')

    # Szukanie
    search_bar.send_keys('selenium')
    search_button.click()

    # Czekanie na stronę
    wait = WebDriverWait(browser, 10)
    element_to_wait_for = (By.CSS_SELECTOR, '.status-msg-body')
    wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))

    # Pobranie listy tytułów
    list_of_titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 3 elementy
    assert len(list_of_titles) == 20



def test_post_count_on_2016_label(browser):

    # Inicjalizacja elementu z labelką
    get_year = browser.find_element(By.PARTIAL_LINK_TEXT, '2016')

    # Kliknięcie na labelkę
    get_year.click()

    # Pobranie listy tytułów
    list_of_titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 1 element
    assert len(list_of_titles) == 24
