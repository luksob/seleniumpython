from pages.home import HomePage
import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from pages.search_result import SearchResultPage


@pytest.fixture
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('https://awesome-testing.blogspot.com')
    yield browser
    browser.quit()


def test_post_count(browser):
    home_page = HomePage(browser)
    home_page.verify_post_count(4)


def test_post_count_after_search(browser):
    home_page = HomePage(browser)
    home_page.search_for('selenium')

    search_result_page = SearchResultPage(browser)
    search_result_page.wait_for_load()
    search_result_page.verify_post_count(20)

    # search_bar = browser.find_element(By.CSS_SELECTOR, 'input.gsc-input')
    # search_button = browser.find_element(By.CSS_SELECTOR, '.gsc-search-button')
    #
    # search_bar.send_keys('selenium')
    # search_button.click()
    #
    # wait = WebDriverWait(browser, 10)
    # element_to_wait_for = (By.CSS_SELECTOR, '.status-msg-body')
    # wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))

    # list_of_titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    #
    # assert len(list_of_titles) == 20


def test_post_count_on_2016_label(browser):
    home_page = HomePage(browser)
    home_page.click_label('2016')
    search_result_page = SearchResultPage(browser)
    search_result_page.verify_post_count(24)

    # get_year = browser.find_element(By.PARTIAL_LINK_TEXT, '2016')
    # get_year.click()
    # list_of_titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    # assert len(list_of_titles) == 24
