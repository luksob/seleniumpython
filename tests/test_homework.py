
import pytest
from selenium.webdriver.chrome import webdriver
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_login_page import ArenaLoginPage
from pages.arena.arena_project_page import ArenaProjectPage
from utils.random_message import generate_random_text


@pytest.fixture
def browser():
    driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
    driver.get('http://demo.testarena.pl/zaloguj')
    driver.maximize_window()
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.log_in_with_valid_credentials('administrator@testarena.pl', 'sumXQQ72$L')
    yield driver
    driver.quit()


def test_add_new_project_and_verify_if_its_added_on_projects_list(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()
    arena_project_page = ArenaProjectPage(browser)
    arena_project_page.click_add_project_button()

    random_project_name = generate_random_text(10)
    random_project_prefix = generate_random_text(4)
    random_project_description = generate_random_text(50)

    arena_project_page.insert_project_name(random_project_name)
    arena_project_page.insert_project_prefix(random_project_prefix)
    arena_project_page.insert_project_description(random_project_description)
    arena_project_page.click_save_button()

    # project_success_added_popup = browser.find_element(By.CSS_SELECTOR, '#j_info_box p')
    # assert project_success_added_popup.text == 'Projekt został dodany.'
    arena_project_page.wait_for_success_message_on_popup()
    arena_project_page.verify_project_success_added_popup()

    arena_project_page.click_projects_tab_from_administration_page()
    arena_project_page.insert_text_to_search_input(random_project_name)
    arena_project_page.click_search_button()
    project_results_list = browser.find_element(By.CSS_SELECTOR, 'tbody > tr > td')
    assert project_results_list.text == random_project_name  # pytanie, czy lepiej asercje trzymać w POP? Dla mnie jest czytelniejsze po za POP, ale wtedy trzeba zmieniać za każdym razem w przypadku zmiany w asercji.
